package com.tugas;

public class Worker2 {
    private int id;
    private String nama;
    private String jabatan;
    private String kendaraan;
    private int in;
    private int out;
    private int harga;

    Worker2(int id, String nama, String jabatan, String kendaraan, int in, int out) {
        this.id = id;
        this.nama = nama;
        this.jabatan = jabatan;
        this.kendaraan = kendaraan;
        this.in = in;
        this.out = out;
    }

    public int getId() {
        return id;
    }

    public String getNama() {
        return nama;
    }

    public String getJabatan() {
        return jabatan;
    }

    public String getKendaraan() {
        return kendaraan;
    }

    public int getIn() {
        return in;
    }

    public int getOut() {
        return out;
    }

    public int hitungHarga() {
        int selisih = this.out - this.in;
        if (this.kendaraan.equals("Motor")) {
            this.harga = (selisih - 1) * 1000 + 2000;

        } else if (this.kendaraan.equals("Mobil")) {
            this.harga = (selisih - 1) * 3000 + 5000;

        }
        return harga;
    }

    public void show() {
        System.out.println(getId() + "\t\t" + getNama() + "\t\t" + getJabatan() + "\t\t" + getKendaraan() + "\t\t" + getIn() + "\t\t" + getOut() + "\t\t" + hitungHarga());
    }

    public static void main(String[] args) {
        Worker2 worker2[] = {
                new Worker2(1, "John", "Manager", "Mobil", 10, 13),
                new Worker2(2, "Peter", "Staff", "Motor", 12, 14),
                new Worker2(3, "Linda", "Staff", "Motor", 13, 16),
                new Worker2(4, "Lucy", "Manager", "Mobil", 8, 16)
        };
        System.out.println("ID\t\tNama\t\tJabatan\t\tKendaraan\tIn\t\tOut\t\tHarga");

        for (Worker2 data : worker2) {
            data.show();
        }
    }
}
