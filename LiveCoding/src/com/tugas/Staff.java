package com.tugas;

public class Staff{
    private int hargaParkirManager = 10000;
    private int hargaParkirStaff = 5000;

    public int getHargaParkirManager() {
        return hargaParkirManager;
    }

    public int getHargaParkirStaff() {
        return hargaParkirStaff;
    }
}
