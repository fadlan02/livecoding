package com.tugas;

import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;
import java.text.ParseException;
import java.time.LocalTime;
import java.time.format.DateTimeFormatter;

public class Worker {
    private int id;
    private String nama;
    private String jabatan;
    private String kendaraan;
    private String in;
    private String out;
    private int harga;

    public Worker(int id, String nama, String jabatan, String kendaraan, String in, String out) throws ParseException, IOException, IOException {
        this.id = id;
        this.nama = nama;
        this.jabatan = jabatan;
        this.kendaraan = kendaraan;
        this.in = in;
        this.out = out;

//        SimpleDateFormat format = new SimpleDateFormat("HH:mm");
//        Date waktuMasuk = format.parse(in);
//        Date waktuKeluar = format.parse(out);
//        // milisecond
//        long selisihMili = waktuKeluar.getTime() - waktuMasuk.getTime();
//      System.out.println(selisihMili);
    }

    public int getId() {
        return id;
    }

    public String getNama() {
        return nama;
    }

    public String getJabatan() {
        return jabatan;
    }

    public String getKendaraan() {
        return kendaraan;
    }

    public String getIn() {
        return in;
    }

    public String getOut() {
        return out;
    }

    public int getHarga() {
        DateTimeFormatter format = DateTimeFormatter.ofPattern("HH:mm");
        LocalTime waktuMasuk = LocalTime.parse(in, format);
        LocalTime waktuKeluar = LocalTime.parse(out, format);

        long selisihJam = java.time.Duration.between(waktuMasuk, waktuKeluar).toHours();
//        System.out.println(selisihJam);

        if (this.kendaraan.equalsIgnoreCase("Mobil")) {
            this.harga = (int) (selisihJam - 1) * 3000 + 5000;
        } else if (this.kendaraan.equalsIgnoreCase("Motor")) {
            this.harga = (int) (selisihJam - 1) * 1000 + 2000;
        }
        return harga;
    }
    public void show() {
        System.out.println(id + "\t\t" + nama + "\t\t" + jabatan + "\t\t" + kendaraan + "\t\t" + in + "\t\t" + out + "\t\t" + getHarga());
    }
}

